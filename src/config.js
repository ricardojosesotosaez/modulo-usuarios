export const mainPass = "2msD2WvAysbADjGGnQTsnzZvTkZ6or5L";


export const appConf = {
    "confMenu": {
        "opciones": [
            ["Inicio", "/"],
            ["Usuarios", "/usuarios"],
            ["Roles", "/roles"],
            ["Funcionalidades", "/funcionalidades"],
            ["Procesos de Negocio", "/procesosnegocio"],
            ["Perfiles", "/perfiles"],
            ["Parámetros", "/parametros"],
            ["Reportes", 
                ["Accesos por Usuario", "/AccesosPorUsuario"],
                ["Accesos por Funcionalidad", "/AccesosPorFuncionalidad"],
                ["Privilegios Efectivos","/PrivilegiosEfectivos"]
            ],
            ["Privilegios al Botón", "/privilegios_Boton"],
            ["Grupo de Menú", "/grupo_Menu"],
            ["Login", "/login"],
            ["Seguridad", "/seguridad"],

        ],
        "orientacion": "vertical",
        "showUserData": true
    },
    mainApi: "/",
    baseUrl: ""
};

