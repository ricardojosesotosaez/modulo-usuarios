import 'core-js';

import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import 'react-datepicker/dist/react-datepicker.css';
import './css/bootstrap.min.css';
import './css/style.css';
import './css/all.css';

// Pantallas
import Inicio from './components/app/Inicio';
import Usuarios from './components/app/Usuarios';

import NotFound from './components/NotFound';

const Root = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={Inicio} />
                <Route exact path="/usuarios" component={Usuarios} />
                <Route component={NotFound} />
            </Switch>
        </Router>
    )
}

render(<Root/>, document.querySelector('#main'));
