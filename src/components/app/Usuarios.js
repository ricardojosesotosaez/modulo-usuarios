import React, { Component } from 'react';

// Components
import { Main, Title, Section, Row, Column, Button, Input, InputFile, Radio, Checkbox, Grid } from '../sondanet';

import UsuarioDetalle from './UsuarioDetalle';

class Usuarios extends Component {

    constructor(){
        super();
        
        this.state = {
            data: {sincronizar: true}
        }
    }
    
    render() {

        // TODO obtener desde API
        let usuarios = [
            {"idusuario":"SONDA_USUARIOS\\carolinag","nombre":"Maribeth","cargo":"GERENTE","estado":"VI","agencia":"SUCURSAL LA FLORIDA"},
            {"idusuario":"SONDA_USUARIOS\\jarias","nombre":"Bevan","cargo":"GERENTE","estado":"VI","agencia":"SUCURSAL LA FLORIDA"},
            {"idusuario":"SONDA_USUARIOS\\cperez","nombre":"Sammy","cargo":"GERENTE","estado":"VI","agencia":"SUCURSAL LA FLORIDA"},
            {"idusuario":"SONDA_USUARIOS\\dburgos","nombre":"Rosanna","cargo":"GERENTE","estado":"VI","agencia":"SUCURSAL LA FLORIDA"},
            {"idusuario":"SONDA_USUARIOS\\pandino","nombre":"Brander","cargo":"GERENTE","estado":"VI","agencia":"SUCURSAL LA FLORIDA"}
        ];
        
        return (
            <Main>
                <Button text="Nuevo usuario" />                
                <Grid id="listadoUsuarios" dataSource={usuarios} pagination paginationSize="10" scrollable scrollHeight="200" showDetail searchable >
                    <columns>
                        <col name="idusuario" title="Id. Usuario" />
                        <col name="nombre" title="Nombre" />
                        <col name="cargo" title="Cargo" />
                        <col name="estado" title="Estado" />
                        <col name="agencia" title="Agencia" />
                    </columns>
                    <detail>
                        <UsuarioDetalle />
                    </detail>
                </Grid>
                {
                    this.state.data && this.state.data.sincronizar
                    ?
                    <div>
                        <Button text="Sincronizar"/>
                        <Button text="Sincronizar todo"/>
                    </div>
                    :
                    null
                }

            </Main>
        );
    }
}

export default Usuarios;