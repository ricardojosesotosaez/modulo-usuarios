import React, { Component } from 'react';

// Components
import { Main, Title, Section, Row, Column, Button, Input, InputFile, Radio, Checkbox, Login } from '../sondanet';


// TODO: El login queda muy abajo
class Inicio extends Component {
    render() {
        return (
            <Main size="l">
            <Login 
                name="snd-login"
                apiUrl="/sondaapi/api/login"
                successUrl="/ejemplos/estilos"
                idAdm="1"
                dominio="sonda_usuarios"
            />
        </Main>
        );
    }
}

export default Inicio;