import React, { Component } from 'react';

import { Title, List, ListItem, Form, Input, Button, Row, Column, Section, Radio } from '../sondanet';

class UsuarioDetalle extends Component {

	constructor(){
		super();

		this.renderData = this.renderData.bind(this);
	}

	componentDidMount(){
		const data = this.props.data;

		this.setState({
			nombre: data["nombre"]
		});
	}

	renderData(){
		const data = this.props.data;

		// console.log(data);

		// let arrData = [];

		// let count = 0;
		
		// for (const key in data) {
		// 	if (data.hasOwnProperty(key)) {

		// 		data[key]
				
		// 		arrData.push(<ListItem key={count}>{key}: {data[key]}</ListItem>);

		// 		count++;
		// 	}
		// }

		// return arrData;

	}
	
	render() {		
		return (
			<div className="snd-grid-custom-detail-content">

				<Title text="Editar datos del usuario" />
				
				{/* <p>{this.props.data['first_name']}</p> */}
				{/* onSubmit={this.handleSubmit} */}

				<Section sectionTitle="Datos Usuario" expandible>
					<Form name="editarUsuarios" apiUrl="">
						<Row>
							<Column>
								<Input name="idusuario" type="text"  label="Id. Usuario"  value={this.props.data['idusuario']} disabled/>
							</Column>
							<Column>
								<Input name="cargo" type="select" label="Cargo"  />
							</Column>
						</Row>

						<Row>
							<Column>
								<Input name="nombre" type="text" label="Nombre" value={this.props.data['nombre']} />
							</Column>
							<Column>
								<Input name="jefe" type="select" label="Jefe"  />
							</Column>
						</Row>

						<Row>
							<Column>
								<Input name="rut" type="text" label="Rut"  />
							</Column>
							<Column>
								<Input name="agencia" type="select" label="Agencia"  />
							</Column>
						</Row>

						<Row>
							<Column>
								<Input name="telefono" type="text" label="Teléfono"  required />
							</Column>
							<Column>
								<Input name="idinterno" type="text" label="Id. Interno"  />
							</Column>
						</Row>

						<Row>
							<Column>
								<Input name="direccion" type="text" label="Dirección"  />
							</Column>
							<Column>
								<Input name="email" type="email" label="Email"  />
							</Column>
						</Row>

						<Row>
							<Column>
								<Input name="comuna" type="select" label="Comuna"  />
							</Column>
							<Column>
								<Input name="activedirectory" type="text" label="Usuario Active Directory"  />
							</Column>
						</Row>

						<Row>
							<Column>
								<Input name="area" type="select" label="Area"  />
							</Column>
							<Column>
							</Column>
						</Row>

						<Row>
							<Column>
								<label>Bloqueado</label>
								<Radio name="bloqueado" label="Si" selected value="si" />
								<Radio name="bloqueado" label="No" value="no" selected   />
							</Column>
							<Column>
								<label>Estado</label>
								<Radio name="estado" label="Vigente" selected value="vigente" />
								<Radio name="estado" label="No vigente" value="novigente"   />
							</Column>
						</Row>
						

						<Button text="Guardar" dbAction="post" center/>
					</Form>
				</Section>
				
				<Section sectionTitle="Asociar Roles" expandible>

				</Section>				
				<Section sectionTitle="Asociar Perfiles" expandible>
				
				</Section>				
				
				{/* TODO: falta el tipo password */}
				<Section sectionTitle="Cambiar contraseña" expandible>
					<Form name="cambioContrasena" apiUrl="">
						<Input name="contrasenaAnterior" type="text" label="Contraseña Antigua" /> 
						<Input name="contrasenaNueva" type="text" label="Contraseña Nueva" /> 
						<Input name="confirmarContrasenaNueva" type="text" label="Confirmar Contraseña" /> 
					<Button text="Guardar" dbAction="post" center/>
					</Form>
				</Section>
				
			</div>
		);
	}
}

export default UsuarioDetalle;
