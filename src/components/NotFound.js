import React from 'react';
import { Main } from './sondanet';



class NotFound extends React.Component {

    render () {
        return (
            <Main>
                <h1 className="txt-center">Lo sentimos, la página solicitada no existe.</h1>
            </Main>
        )
    }
}

export default NotFound;
