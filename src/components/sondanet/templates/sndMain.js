import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';

import {Menu, Breadcrumbs } from '../../sondanet';

// Config
import { appConf, mainPass } from '../../../config.js';

// Helpers
import { userDataExist, urlDataExist } from '../helpers';

import encrypter from 'object-encrypter';
const encrypterEngine = encrypter(mainPass);

class sndMain extends Component {

	constructor(props) {
		super(props);

		this.goUp = this.goUp.bind(this);
		this.checkUserData = this.checkUserData.bind(this);

		this.state = {
			size: props.size,
			showGoUp: false,
			urlData: undefined,
			userData: undefined
		};
	}

	componentDidMount(){
		window.addEventListener('scroll', ()=>{
			if (window.scrollY > 100){
				this.setState({
					showGoUp: true
				});
			} else {
				this.setState({
					showGoUp: false
				});
			}
		});
		this.checkUserData();
	}

	checkUserData(){
		const userData = userDataExist();
		const self = this;
		if (userData !== null){
			const decUserData = encrypterEngine.decrypt(userData);
			if(decUserData !== null){
				self.setState({ userData: decUserData });
			}
		}
	}

	goUp(){
		let scrollToTop = window.setInterval(() => {
			let pos = window.pageYOffset;
			if (pos > 0) {
				window.scrollTo(0, pos - 20);
			} else {
				window.clearInterval(scrollToTop);
			}
		}, 5);
	}

	render() {

		let childrenComponents;
		if (this.props.children.props !== undefined && this.props.children.props.name !== undefined && this.props.children.props.name === 'snd-login'){
			let login = React.cloneElement(this.props.children, {checkUserData: this.checkUserData} );
			childrenComponents = login;
		} else {
			childrenComponents = this.props.children;
		}
		
		return (
			<div className={"snd-global-container " + appConf.confMenu.orientacion}>
				<Menu 
					logoAlt="Sonda S.A" 
					userData={this.state.userData !== undefined ? this.state.userData : undefined}
					confMenu={appConf.confMenu} 
				/>
				<div className={'snd-container ' + (this.state.size ? this.state.size : '')}>
					<Breadcrumbs />
					{childrenComponents}
					<div className={'up ' + (this.state.showGoUp ? 'show':'')} onClick={() => { this.goUp() }}>
						<FontAwesome name="chevron-up" />
					</div>
				</div>
			</div>
		);
	}
}

sndMain.propsTypes = {
	size: PropTypes.string
}

export default sndMain;