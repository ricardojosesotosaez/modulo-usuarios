import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';

// Config
import { appConf } from '../../../../config';

// Helpers
import { deleteSessionData } from '../../helpers';

class sndUserData extends Component {

	logOut(){
		deleteSessionData('sndUserData');
		window.location.href = appConf.baseUrl;
	}

	render() {
		return (
			<div id="user-data-container">
			{
				this.props.data !== undefined
				?
				<div id="user-data">
					<span className="user-icon"><FontAwesome name="user" /></span>
					<p>{this.props.data.nombre}</p>
							<span className="logout" onClick={() => { this.logOut() }}><FontAwesome name="sign-out-alt" /></span>
				</div>
				:
				null
			}
			</div>
		);
	}
}

sndUserData.propsTypes = {
	data: PropTypes.object
}

export default sndUserData;