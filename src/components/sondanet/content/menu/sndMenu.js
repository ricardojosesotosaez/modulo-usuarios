import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Navbar, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import ScrollArea from 'react-scrollbar';

import UserData from './sndUserData';

import { appConf } from '../../../../config.js';

const baseUrl = appConf.baseUrl;

class sndMenu extends Component {

    constructor(props) {
        super(props);

        this.toggleDropDown = this.toggleDropDown.bind(this);
        this.openMobileMenu = this.openMobileMenu.bind(this);
        this.linkCLick = this.linkCLick.bind(this);
        
        this.state = {
            logoAlt: props.logoAlt,
            confMenu: props.confMenu,
            opciones: [],
            dropDownOpen: false,
            arrDropDownOpen: {},
            vertical: false,
            horizontal: "horizontal",
            tabs: false,
            pills: false,
            justified: false,
            fill: false,
            navbar: false,
            tag: "",
            mobileMenu: false,
            menuItemSel: ""
        };

    }

    componentDidMount(){
        const confMenu = this.state.confMenu;

        let showUserData = false;
        let orientacion = "vertical";

        // Items incluídos en el menú
        let arrOpciones = [],
            opcionObj,
            arrSubOpciones = [],
            subOpcionObj,
            arrDropDownOpen = {};

        let sndMenuPath = sessionStorage.getItem('sndMenuPath') || '';
        let selMenuItem;
        if(sndMenuPath.length){
            selMenuItem = sndMenuPath.split('/')[0];
        }

        for (var key of Object.keys(confMenu)) {
            switch (key) {

                case "orientacion":
                    orientacion = confMenu[key];
                    break;

                case "showUserData":
                    showUserData = confMenu[key] ? true : false;
                    break;

                case "opciones":
                    let idDropDown,
                        idDropItem;

                    confMenu[key].forEach((element, index) => {
                        for (var i = 0; i < element.length; i++) {
                            if (typeof (element[i]) !== "string") {
                                subOpcionObj = <NavItem key={i}><NavLink href={baseUrl + element[i][1]} onClick={(e)=>{this.linkCLick(e)}} className={selMenuItem === element[i][0] ? 'active' : ''}>{element[i][0]}</NavLink></NavItem>;
                                arrSubOpciones.push(subOpcionObj);
                            }
                        }

                        if (arrSubOpciones.length > 0) {
                            idDropDown = "dd-" + index;
                            idDropItem = "di-" + index;
                            if(orientacion === 'vertical'){
                                opcionObj = <NavItem key={idDropItem}><NavLink onClick={(e)=>{this.toggleDropDown(e)}} className={selMenuItem === element[0] ? 'active' : ''}>{element[0]}</NavLink><ul key={idDropDown} id={idDropDown} className={'nav-dropdown ' + (this.state.arrDropDownOpen[idDropDown] ? 'show' : '')}>{arrSubOpciones}</ul></NavItem>;
                            } else {
                                opcionObj = <NavItem key={idDropItem}><NavLink className={selMenuItem === element[0] ? 'active' : ''}>{element[0]}</NavLink><ul key={idDropDown} id={idDropDown} className={'nav-dropdown ' + (this.state.arrDropDownOpen[idDropDown] ? 'show' : '')}>{arrSubOpciones}</ul></NavItem>;
                            }
                            arrDropDownOpen[idDropDown] = false;
                        } else {
                            opcionObj = <NavItem key={index}><NavLink href={baseUrl + element[1]} onClick={(e)=>{this.linkCLick(e)}} className={selMenuItem === element[0] ? 'active' : ''}>{element[0]}</NavLink></NavItem>;
                        }

                        arrOpciones.push(opcionObj);
                        arrSubOpciones = [];

                    }, this);
                    break;

                default:
                    break;
            }
        }

        this.setState({
            opciones: arrOpciones,
            arrDropDownOpen: arrDropDownOpen,
            showUserData: showUserData,
            orientacion: orientacion
        });

        setTimeout(() => {
            let shownItems = document.getElementsByClassName('nav-link');
            for (let i = 0; i < shownItems.length; i++) {
                if(shownItems[i].className.indexOf('active') > -1){
                    this.setState({
                        menuItemSel: shownItems[i].innerHTML
                    });
                    return false;
                }
            }
        }, 10);
    }

    toggleDropDown(e){
        e.preventDefault;

        let shownItems = document.getElementsByClassName('nav-link');
        for (let i = 0; i < shownItems.length; i++) {
            if(shownItems[i].className.indexOf('show') > -1){
                shownItems[i].className = 'nav-link';
            }
        }

        if(e.target.className.indexOf('show') > -1){
            e.target.className = 'nav-link';
        } else {
            if(e.target.className.indexOf('active') === -1){
                e.target.className = e.target.className + ' show';
                this.setState({
                    menuItemSel: e.target.innerHTML
                });
            }
        }

    }

    openMobileMenu(){
        const open = this.state.mobileMenu;
        this.setState({
            mobileMenu: !open
        });
    }

    linkCLick(e){
        let parentClass = e.target.parentNode.parentNode.className;
        let menuPath;
        
        if(parentClass === 'navbar-nav'){
            menuPath = e.target.innerHTML;
        } else {
            menuPath = this.state.menuItemSel + '/' + e.target.innerHTML;
        }
        
        sessionStorage.setItem('sndMenuPath', menuPath);
    }

    render() {
        
        return (
            <div className={"snd-menu " + this.state.orientacion}>
                <span className="snd-mobile-menu" onClick={()=>{this.openMobileMenu()}}>
                    <FontAwesome name="bars" size="2x"/>
                </span>
                <Navbar expand="md">
                    <NavbarBrand href={appConf.baseUrl} />
                    {
                        this.state.orientacion === 'vertical' && this.state.showUserData ?
                        <UserData data={this.props.userData} /> : null
                    }
                    <ScrollArea horizontal={false} stopScrollPropagation={!this.state.mobileMenu} className={this.state.mobileMenu ? 'open' : ''}>
                        <Nav navbar className={this.state.mobileMenu ? 'open' : ''}
                        >
                            {this.state.opciones}
                        </Nav>
                    </ScrollArea>
                    {
                        this.state.orientacion === 'horizontal' && this.state.showUserData ?
                        <UserData data={this.props.userData} /> : null
                    }
                </Navbar>
                {/* <p><FontAwesome name="chevron-left" size="2x"/></p> */}
            </div>
        );
    }
}

sndMenu.propsTypes = {
    confMenu: PropTypes.object.isRequired
}

export default sndMenu;