import React, { Component } from 'react';
import { Button } from '../../sondanet';
import FontAwesome from 'react-fontawesome';

class sndCode extends Component {

	constructor(){
		super();

		this.copyCode = this.copyCode.bind(this);
	}

	copyCode(){
		document.addEventListener('copy', (e)=>{
			e.clipboardData.setData('text/plain', this.props.children);
			e.preventDefault();
		});
		document.execCommand('copy');
	}
	
	render() {
		return (
			<div className="snd-code">
				{
					this.props.title !== undefined && this.props.title.length > 0
					?
						<p className="snd-code-titulo">{this.props.title}</p>
					: null
				}
				<div className="snd-code-container">
					<pre>
						{this.props.children}
					</pre>
					<Button clickAction={this.copyCode} clickActionText="El código de referencia ha sido copiado al portapapeles" text={<FontAwesome name="copy" />}/>
				</div>
			</div>
		);
	}
}

export default sndCode;
