import React, { Component } from 'react';
import PropTypes from 'prop-types';

class sndColumn extends Component {

	constructor(props){
		super(props);

		this.state = {
			size: props.size
		}
	}

	render() {
		let size = "";
		if (this.state.size !== undefined){

			if (this.state.size === "auto"){
				size = "col-md-auto";
			} else if (!isNaN(this.state.size)) {
				size = "col-" + this.state.size;
			}
		}
		
		return (
			<div className={"col " + size}>
				{this.props.children}
			</div>
		);
	}
}

sndColumn.propsTypes = {
	size: PropTypes.string
}

export default sndColumn;