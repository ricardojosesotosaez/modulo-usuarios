import React, { Component } from 'react';

import FontAwesome from 'react-fontawesome';

class sndBreadcrumbs extends Component {

	constructor(){
		super();

		this.state = {
			path: ""
		};
	}

	componentDidMount(){
		let sessionPath = sessionStorage.getItem('sndMenuPath') || "";
		let pathArr = sessionPath.split('/');
		let path = [];

		pathArr.forEach((ele, index) => {
			path.push(<span key={'e-'+index}>{ele}</span>);
			if((index + 1) < pathArr.length){
				path.push(<FontAwesome key={'i-'+index} name="chevron-right" />);
			}
		});
		if(sessionPath.length){
			this.setState({
				path: path
			});
		}
	}
	
	render() {
		return (
			<div className="snd-breadcrumbs-container">
				{
					this.state.path ?
					<div className="snd-breadcrumbs">
						<span><FontAwesome name="home" /> <FontAwesome name="chevron-right" /> </span>
						<span>{this.state.path}</span>
					</div>
					: 
					null
				}
			</div>
		);
	}
}

export default sndBreadcrumbs;
