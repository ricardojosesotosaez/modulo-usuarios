import React, { Component } from 'react';

class sndListItem extends Component {

	render() {
		return (
			<li className="list-group-item">
				{this.props.children}
			</li>
		);
	}
}

export default sndListItem;