import React, { Component } from 'react';
import PropTypes from 'prop-types';

class sndTitle extends Component {

	render() {
		return (
			<header>
				<h1>{this.props.text}</h1>
			</header>
		);
	}
}

sndTitle.propsTypes = {
	text: PropTypes.string.isRequired
}

export default sndTitle;