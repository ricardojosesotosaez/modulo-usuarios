import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';

class sndSection extends Component {

	constructor(props){
		super(props);

		this.toggleExpansion = this.toggleExpansion.bind(this);

		this.state = {
			expanded: this.props.expandible ? false : true
		};
	}

	toggleExpansion(){
		const expanded = this.state.expanded;
		this.setState({
			expanded: !expanded
		});
	}

	render() {
		return (
			<section className={(this.props.expandible ? 'expandible' : '') + (this.props.expandible && this.state.expanded ? ' expanded' : '')}>
			{
					this.props.sectionTitle !== undefined && this.props.sectionTitle.length > 0
					?
						<header>
							{
								this.props.expandible 
								? 
								<div className="snd-section-btn" onClick={() => {this.toggleExpansion()}}>
									<FontAwesome name={this.state.expanded ? 'minus-circle' : 'plus-circle'} />
								</div> 
								: 
								null
							}
							<h1 onClick={() => {this.toggleExpansion()}}>{this.props.sectionTitle}</h1>
						</header>
					: null
			}
				{this.props.children}
			</section>
		);
	}
}

sndSection.propsTypes = {
	sectionTitle: PropTypes.string.isRequired
}

export default sndSection;