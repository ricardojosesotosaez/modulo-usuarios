import React from 'react';

class Loading extends React.Component {
	render() {
		return (
			<div className="loading-overlay">
				<div className="loading">
					<div className="spinner"></div>
				</div>
				{
					this.props.text !== undefined && this.props.text.length > 0
					?
					<h5 className="txt-center">{this.props.text}</h5>
					:
					null
				}
			</div>
		)
	}
}

export default Loading;