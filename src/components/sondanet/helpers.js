import { mainPass } from '../../config';
import encrypter from 'object-encrypter';

const encrypterEngine = encrypter(mainPass);

/* Función que permite mostrar una ventana modal.
   Entradas:
   text = variable de tipo string con el texto que se desea desplegar.
   modo = permite seleccionar el modo de la ventana, las opciones son: iframe, normal, question, accept.
   callback = función que se desea ejecutar posteriormente
*/
export function modal(contenido, modo, callback) {
	// Valida si existe contenido para mostrar
	if (arguments[0] === undefined) {
		console.log("[MODAL] No tengo nada que mostrar.");
	} else {
		// Valida que se haya especificado un modo
		if (arguments[1] === undefined) {
			console.log("[MODAL] Faltó el modo.");
		} else {
			// Valida que se haya especificado un modo que exista
			if (arguments[1] !== 'iframe' && arguments[1] !== 'normal' && arguments[1] !== 'question' && arguments[1] !== 'accept') {
				console.log("[MODAL] El modo seleccionado no existe, por favor verificar.");
			} else {
				// Fondo del modal
				var modalOverlay = document.createElement('div');
				modalOverlay.id = 'modal-overlay';

				// Contenedor para lo que se desea desplegar
				var modalContainer = document.createElement('div');
				modalContainer.id = 'modal-container';

				// Boton para cerrar el modal
				var btnCerrarModal = document.createElement('span');
				btnCerrarModal.id = 'btn-cerrar-modal';

				// Evento para cerrar la ventana modal con el botón
				btnCerrarModal.addEventListener('click', function () {
					// Verifica que el contenedor principal del modal sea visible
					if (modalOverlay.offsetParent === null) {
						document.body.removeChild(modalOverlay);
					}
				});

				// Contenido que se va a desplegar
				var modalContent = '';
				switch (modo) {
					// Modo de normal
					case 'normal':
						modalContent = document.createElement('p');
						modalContent.innerHTML = contenido;
						break;
					// Modo de Iframe
					case 'iframe':
						modalContent = document.createElement('iframe');
						modalContent.src = contenido;
						break;
					// Modo de texto de confirmación o aceptación
					case 'question': case 'accept':
						// Valida que, en el caso que se haya especificado el modo de confimación, se haya
						// especificado la función callback para el boton 'Aceptar'
						if (modo === 'question' && callback === undefined) {
							console.log("[MODAL] Debe definir una función callback para el botón 'Aceptar'");
						} else {
							modalContent = document.createElement('div');
							var cont = document.createElement('p');
							cont.innerHTML = contenido;

							// Contenedor de los botones
							var contBotones = document.createElement('div');
							contBotones.id = 'cont-botones';

							// Los botones
							var btnAceptar = document.createElement('button');
							btnAceptar.className = 'button aceptar';
							btnAceptar.innerHTML = 'Aceptar';

							// Evento del botón 'Aceptar'
							if(modo === 'question'){
								btnAceptar.addEventListener('click', callback);
							} else {
								btnAceptar.addEventListener('click', function(){ document.body.removeChild(modalOverlay); });
							}

							// Agrega botones contBotones
							contBotones.appendChild(btnAceptar);

							// Para el modo question se agrega el botón Cancelar
							if(modo === 'question'){
								var btnCancelar = document.createElement('button');
								btnCancelar.className = 'button cancelar';
								btnCancelar.innerHTML = 'Cancelar';
								
								// Evento del botón 'Cancelar'
								btnCancelar.addEventListener('click', function () {
									if (modalOverlay.offsetParent === null) {
										document.body.removeChild(modalOverlay);
									}
								});

								contBotones.appendChild(btnCancelar);
							}

							modalContent.appendChild(cont);
							modalContent.appendChild(contBotones);
						}
						break;
					default:
						break;
				}
				// Se agregan los elementos correspondientes
				if(modo !== 'accept'){
					modalContainer.appendChild(btnCerrarModal);
				}
				modalContainer.appendChild(modalContent);
				modalOverlay.appendChild(modalContainer);

				// Agrega el contenido al 'body'
				document.body.appendChild(modalOverlay);
			}
		}
	}
}

// Muestra un cuadro de mensaje emergente que desaparece automáticamente
export function toast(text){
	let toasters = document.getElementsByClassName('snd-toast-container');

	if (toasters.length > 0){
		for (let i = 0; i < toasters.length; i++) {
			toasters[i].remove();
		}
	}

	let vertical = document.getElementsByClassName('vertical');
	let toastContainerClass;
	if(vertical.length){
		toastContainerClass = 'snd-toast-container vertical';
	} else {
		toastContainerClass = 'snd-toast-container horizontal';
	}
	
	let container = document.createElement('div');
	container.className = toastContainerClass;
	let p = document.createElement('p');
	p.innerHTML = text;
	container.appendChild(p);
	document.body.appendChild(container);
	let duration = 60000 * (text.split(' ').length / 200);
	duration = duration < 4000 ? 4000 : duration;

	setTimeout(() => {
		container.className = toastContainerClass + ' show';
	}, 10);
	
	setTimeout(() => {
		container.className = toastContainerClass;
	}, duration+10);

	setTimeout(() => {
		container.remove();
	}, duration + 210);
}

// Insertar valores en cierta posición de un string
export function insert(str, index, value) {
	return str.substr(0, index) + value + str.substr(index);
}

// Retorna un uuid generado
export function uuidGen(a) {
	return a ? (a ^ Math.random() * 16 >> a / 4).toString(16) : ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, uuidGen);
}

// Revisa si existe información de usuario
export function userDataExist(){
	let userData = sessionStorage.getItem('sndUserData');
	return userData;
}

// Revisa si existe información de datos
export function getUrldata(){
	return encrypterEngine.decrypt(localStorage.getItem('sndUrlData'));
}

export function deleteUrldata(){
	return localStorage.removeItem('sndUrlData');
}

// Elimina datos del usuario de la sesion local
export function deleteSessionData(name){
	sessionStorage.removeItem(name);
}

// "Trimea" los elementos en un arreglo de strings
export function cleanArray(arr){
	for (let i = 0; i < arr.length; i++) {
		arr[i] = arr[i].trim();
	}

	return arr;
}

// Obtiene parámetro que se pasa el final de una URL como 'llave' para ser usada en otra pantalla
export function getUrlKey(){
	let urlArr = window.location.href.split('/');
	return urlArr[urlArr.length - 1];
}