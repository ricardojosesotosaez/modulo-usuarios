import React, { Component } from 'react';
import request from 'superagent';

import { Input, Button, Loading } from '../../sondanet';
import { toast, userDataExist } from '../helpers';
import { appConf, mainPass } from '../../../config';

import encrypter from 'object-encrypter';
const encrypterEngine = encrypter(mainPass);

const logoSonda = "data:image/gif;base64,R0lGODlheAArAPcAAAAAAAoKChISEhoaGiMjIysrKzMzMzw8PEFBQUZGRkpKSk1NTVJSUllZWV5eXmVlZWxsbHR0dHh4eH9/fwY9gAk/gQxCgxFFhRRIhxhKiRtNix5QjCJTjiZWkCtakzBelTNglzZimDllmj5onEFqnURtn0lyok11pFB3plJ4p1R6qFh9qlyArGCDrmSGsGaIsWmKs26OtXGRt3WUuXiWunqYu3uYvH2avYaGhouLi5GRkZaWlpmZmZ6enqKioqWlpampqa6urrKysra2trq6ur6+voSfwIahwYehwoqkw4ylxI6nxZCoxpWsyJivypmxzp60zaG2z6K30KS40ae70qm81K2/1K/B17LD2LbG2rfI3LjH27nI27vK3LzL3b/N3cLCwsXFxcjIyM3NzdHR0dLS0tPT09TU1NTU1dbW1tfX19jY2NnZ2dra2tvb29zc3N3d3d7e3t/f38HP4MLQ4MTR4crW5M7a59Pd6dfg69nh7Nvj7dzk7d/m7uDg4OHh4eLi4uPj4+Tk5OXl5ebm5ufn5+jo6Onp6erq6uvr6+zs7O7u7u/v7+Hn8OTq8ujt8+rv9ezx9vDw8PHx8fLy8vPz8/T09PT09fb29vf39/H0+PX3+vf5+/j4+Pn5+fr6+vv7+/j6/Pr7/fz8/P39/fz9/v7+/v7+/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwAAAAAeAArAAAI/wBPCRxIsKBBg4wGtRkipOGZRJ5MmTp4ypIiN0MyrlF0ieKoSZQoTap0sBJIkKMGhlzJEtOniRQJfpo50xOpmDhjFnkwIIDPnwQkvDm4SAiDnj8DDGgQhJFBMAcQIDiwwFDBSwukHmCQRmAgqWDDOoigg40lnGYiqF3rI6fbU6BATYoAoK7duwAGhClI5ADevwnGEDSFA2+EgkUC2C3Q9ZSPv38DNFBD8VCCvwduvo1pClRhyHgNZBrYQzHouwXaDJTUAK8AygNz3GUw6dSoB6fxFiBzUALkAlY3x0Q04O6BCT1wXL4r+BQQ03YVSMghwQDeBiRPwSnw9wFBBXdxCP/0xN1ugAIGDBDAHHwgEeh3B4gRHlMQXh0D4zRQ8ADCBEKnsLGeeTssMpAa1pnXHBiQCVCEQJ0MWBcQAvkBXwNkrMHGGD2UZ1cPBCHiIV4B8EAfRfbdRQAERNRWCSKgaAaKb3dNYNBjd7V1Cg+gMfDJKWIIYBcB853yA14gEgSEkHY1QBAEdzXgl12HnYhQgncJUEADPmQn0B9YAlDAaAXJgdcOp3SC25DmEXGKDtApUNspdJkHRkGMTFlXAgMNAV0AYaxZlwNWGmQKGMVBptQDiAgUBnxVFuQHXiYmMuIOWDoJpV0QDKSnmGwUVMlydSkgUCJhOmDKBHchIEmhBpn/8QABTP7FACanEHHmQUPgNcQpaeDVRhB3AUEqAPidUkiidTVASUFtjLjAKaTUWVcBoeqA2hqwHjTKEBAYwKxdZeSK5EGb1iXAUD2gBqOeBdQ6wJ25wideQTyGd4oQtQKwQBhg4ACdAL/CKgkYRBABhA+KCGRKGK3dFcQpDEb5VL8PXEJKugA40BEY8NllgFOnfFZXAEIUtEaYA7xRSJi5ofkWJHdg8YUoAhlSQABM2jgQsXc9GEiYAfxAEBEjCrAXJiMmm0nEeDEw0AIqgnHJJJKU4QPMEpzCcW4AdH0QJ3hMgYQNSSwxxRV24CyQhAAI8EAOOUQw4gAAmqJtlnPn//BAvwDgAMopgMDnpkDv/eXzIkkfkIBUI9Z1wCVBhJwboQNBggUNJ8CgRBRceFFHHpoYZC3YO8AkCXhg1xXBWacAXRcB3I73tQCHEwF4bgjIIQjcAQCR8PBwI0CKHTSQ0IEISdihByQp4TSJA2ALgEP0Ag0CNWgE9ECm13ct0PBAYoxLgCACydY6AATk0KigdXlXEOt1DWDCBRTk78EVnJSyWSZAWIAABJAUAQzgAWiACUEyIYQGDLCACfABgAiCAwVYEAE80IzDcnCAC3ZKIDpAgAVHSEIFOAAHQ2jUKdywABIyYIIE0cEIE8AADOTvhhXggAlsYAU75KERjniEJv+G6L9uGSQMYgjDGDR2EFJIxBQaLIgToahAUkzxiVhU4EBIMYosUuSJVsRCC0bAAfzd8IwaWN4K1kiDJCABCVXIQha04AU74OGOd8jDIxzBR0dEwoqANKIgB0kRTdRBCjeIAQpMAIIPbMACFjijJCVZAQxc4AIW4EAJSMDJEaAgBqAM5RKeQEpSOiEKW+CCKle5BS/oIQ+wjCUfQsGJWnKCkLjkRCTygIcvVOEKUEiCMI0AAxcY05goEMEIRBCCEDBTAxWApDQnSc38VcCa0sRAM7cZAhCIYI3ghEENbGCDGzRhClGQQh3sUAc9aCISuBRkKERBT1FEgg99yGcfGsH/BzvMoQ4ABagvqUBQgkJhnOS0gQtMwNATfEADG3hkNSeaPwtEswMd4AAINpkCFrQAbU/Iwhf48Ih4mvREm4CESiPRCDzA8g50iOkUltAEI7TABSoAAQhsSNGJWgCiHvhACVIgAyX0EJ4nTWq3SLGJTWjiEY9oxB7ygIUqMEEGLijBCDBp0Z6e0QIYSOMHWmAEKzjCbUpNqxFHIQpOOPURdahCEmawAkda8ppejaY2Z4AFSIRCrYA16fGwcAQWjMADGfDqDTHggjqgNbCQJaQjsGADE3BAsRSwgBEiy1mTfmEGG1CsBTpLWlxaQQNe5UBpV2vELaCWohZwAmtnS59Q/zThshQFwRUeS9veFkQUkGCCCCJZzQp4wAiO8K1yCbKJRkxhBYkt7gZMAIVNLHe5NJNCC6JLTQt0oARGyMN1fbsJPFzBCCcgLjW/+wIn4GG8tOWEHaiABBZ8oJoX+AAKZuCEOpQOvquFqxMU6QG8fvUDKpBBE7aAh/8CuLOasAMUYmCCEOB2sSFIQQyWsIV2IvXBkSUFJLowYBJ4YANmpIAmYQBH5/nxliAG7Fsb4YUmVBYEG9CAjkFgghgkwQp4gIQmNsHbGJs0FJpwBB/uUIcrPIEGMDDxBz7AAhgwAQpf6IMoimhkwPbhDneYAx2i0IQjxIAFJYABDZxQBTv0IRWKXe4sKb4AhSgYgQpVyDIk4iyQgAAAOw==";

class sndLogin extends Component {

	constructor(props){
		super(props);

		this.login = this.login.bind(this);
		this.changePassword = this.changePassword.bind(this);
		this.resetPassword = this.resetPassword.bind(this);
		this.toggleChangePassword = this.toggleChangePassword.bind(this);

		this.state = {
			apiUrl: props.apiUrl,
			idAdm: props.idAdm,
			dominio: props.dominio ? props.dominio : 'no-domain',
			disableInputs: false,
			showLoader: false,
			successUrl: props.successUrl,
			newPassword: "",
			mainPass: appConf.mainPass,
			showChangePassword: false,
			idUsuario: undefined
		};
	}

	login(e){
		e.preventDefault();
		
		let apiUrl = this.state.apiUrl,
			inputs = document.getElementsByClassName('snd-input'),
			values = [];

		for (let i = 0; i < inputs.length; i++) {
			if (inputs[i].value.length === 0){
				toast("Debe completar todos los campos.");
				return false;
			}
			values.push([inputs[i].name, inputs[i].value]);
		}

		this.setState({
			showLoader: true
		});

		let id_usuario, password;
		values.forEach(val => {
			if(val[0] === 'id_usuario'){
				id_usuario = val[1];
			}

			if (val[0] === 'password') {
				password = val[1];
			}
		});

		let reqUrl = apiUrl + '/';
			reqUrl += this.state.idAdm + '/';
			reqUrl += this.state.dominio + '/';
			reqUrl += id_usuario + '/';
			reqUrl += encodeURIComponent(password);

		const self = this;
				
		request
			.get(reqUrl)
			.then((res) => {
				/*
					res.statusCode: Código que retorna la API de login de usuarios
					200: OK
					201: Expiración de clave
					202: Usuario o contraseña inválidos
				*/
				if (res.statusCode !== 200){
					if(res.statusCode === 201){
						self.setState({idUsuario: id_usuario});
						this.toggleChangePassword();
					} else {
						toast(res.text);
					}
				} else {
					let obj = JSON.parse(res.text)[0];
					const encUserData = encrypterEngine.encrypt(obj);
					sessionStorage.setItem('sndUserData', encUserData);
					self.props.checkUserData();
				}
				
				self.setState({
					showLoader: false
				});
			})
			.catch((error) => {
				toast('Ha ocurrido un error procesando la solicitud.');
				self.setState({
					showLoader: false
				});
				console.error(error);
			});
	}

	toggleChangePassword(){
		const showChangePassword = this.state.showChangePassword; 
		this.setState({
			showChangePassword: !showChangePassword
		});
	}

	changePassword(e){
		e.preventDefault();
		
		let apiUrl = this.state.apiUrl,
			inputs = document.getElementsByClassName('snd-input'),
			values = [];

		for (let i = 0; i < inputs.length; i++) {
			if (inputs[i].value.length === 0){
				toast("Debe completar todos los campos.");
				return false;
			}
			values.push([inputs[i].name, inputs[i].value]);
		}

		let old_password, new_password, confirm_new_password;
		values.forEach(val => {
			if(val[0] === 'old_password'){
				old_password = val[1];
			}

			if (val[0] === 'new_password') {
				new_password = val[1];
			}

			if (val[0] === 'confirm_new_password') {
				confirm_new_password = val[1];
			}
		});

		if(new_password !== confirm_new_password){
			toast('Verifique la confirmación de la nueva contraseña.');
			return false;
		}

		this.setState({
			showLoader: true
		});

		let reqUrl = apiUrl + '/';
			reqUrl += this.state.idAdm + '/';
			reqUrl += this.state.dominio + '/';
			reqUrl += this.state.idUsuario + '/';
			reqUrl += encodeURIComponent(old_password) + '/';
			reqUrl += encodeURIComponent(new_password) + '/';
			reqUrl += 'change-password';

		const self = this;
				
		request
			.get(reqUrl)
			.then((res) => {
				/*
					res.statusCode: Código que retorna la API de login de usuarios
					200: OK
					201: Error con las contraseñas ingresadas
				*/
				if (res.statusCode !== 200){
					toast(res.text);
				} else {
					let obj = JSON.parse(res.text)[0];
					const encUserData = encrypterEngine.encrypt(obj);
					sessionStorage.setItem('sndUserData', encUserData);
					self.props.checkUserData();
				}
				
				self.setState({
					showLoader: false
				});
			})
			.catch((error) => {
				toast('Ha ocurrido un error procesando la solicitud.');
				self.setState({
					showLoader: false
				});
				console.error(error);
			});
	}

	resetPassword(){
		toast('No se encuentra habilitada esta funcionalidad por el momento.');
		// let apiUrl = this.state.apiUrl,
		// 	inputs = document.getElementsByClassName('snd-input');

		// let id_usuario;
		// for (let i = 0; i < inputs.length; i++) {
		// 	if (inputs[i].name === "id_usuario" && inputs[i].value.length === 0) {
		// 		toast("Debe ingresar el ID de usuario.");
		// 		return false;
		// 	} else if (inputs[i].name === "id_usuario") {
		// 		id_usuario = inputs[i].value;
		// 	}
		// }
		
		// this.setState({
		// 	showLoader: true
		// });

		// let reqUrl = apiUrl + '/';
		// 	reqUrl += this.state.idAdm + '/';
		// 	reqUrl += this.state.dominio + '/';
		// 	reqUrl += id_usuario + '/reset-password';

		// const self = this;

		// request
		// 	.get(reqUrl)
		// 	.then((res) => {
		// 		if(res.statusCode === 200){					
		// 			modal(res.text, "normal");
		// 		} else {
		// 			toast(res.text);
		// 		}
		// 		self.setState({
		// 			showLoader: false
		// 		});
		// 	})
		// 	.catch((error) => {
		// 		console.log(error);
		// 	});
	}

	render() {
		let vh = window.innerHeight;
		
		return (
			<div className="snd-login-container" style={{height:(vh-40)+'px'}}>
				<div className="snd-login">
					{
						this.state.showLoader
						?
						<Loading />
						:
						<div className="snd-login-form">
							<figure>
								<img src={logoSonda} alt="Sonda S.A" />
							</figure>
							{
								userDataExist() === null
								?
								<div>
									{
										!this.state.showChangePassword
										?
										<div>
											{/* Login */}
											<form noValidate id="formLogin" name="formLogin" className="snd-form s" onSubmit={(e) => { this.login(e) }}>
												<Input type="login-usuario" name="id_usuario" label="ID Usuario" focus />
												<Input type="password" name="password" label="Contraseña" />
												<Button text="Ingresar" dbAction="post" center />
											</form>
											<span className="reset-password" onClick={() => { this.resetPassword() }}>Olvido su contraseña?</span>
										</div>
										:
										<div>
											{/* Cambiar password por expiración */}
											<h5 className="txt-center">Su clave ha expirado. Por favor, ingrese una nueva contraseña.</h5>
											<form noValidate id="formLogin" name="formLogin" className="snd-form s" onSubmit={(e) => { this.changePassword(e) }}>
												<Input type="password" name="old_password" label="Antigua contraseña" focus />
												<Input type="password" name="new_password" label="Nueva contraseña" />
												<Input type="password" name="confirm_new_password" label="Confirmar nueva contraseña" />
												<Button text="Guardar" dbAction="post" center />
											</form>
										</div>
									}
								</div>
								:
								<h4 className="txt-center">Sesión iniciada con éxito</h4>
							}
						</div>
					}
				</div>
			</div>
		);
	}
}

export default sndLogin;
