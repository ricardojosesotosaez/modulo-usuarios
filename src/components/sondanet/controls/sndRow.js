import React, { Component } from 'react';

class sndRow extends Component {

	render() {
		return (
			<div className="row">
				{this.props.children}
			</div>
		);
	}
}

export default sndRow;