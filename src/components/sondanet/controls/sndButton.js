import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';

import { modal, toast } from '../helpers';

class sndButton extends Component {

	constructor(props){
		super(props);

		this.doClickAction = this.doClickAction.bind(this);
		this.generateContent = this.generateContent.bind(this);
		
		this.state = {
			id: props.id,
			type: props.type,
			className: 'button',
			clickAction: props.clickAction !== undefined ? props.clickAction : null,
			dbAction: props.dbAction !== undefined ? props.dbAction : '',
			text: props.text,
			url: props.url,
			callback: props.callback
		};
	}

	componentWillMount(){
		
		let className = this.state.className + ' ';
		let clickAction = this.state.clickAction;
		let id = this.state.id;

		if (this.state.type !== undefined && (this.state.type).indexOf("modal") > -1){
			switch (this.state.type) {
				case 'modal-normal':
					if (this.state.text !== undefined){
						clickAction = () => { modal(this.state.text, "normal");};
					} else {
						console.log("[MODAL] Al botón " + (id !== undefined ? '#' + id + ' ': '') + "le falta la propiedad 'text'");
					}
					break;

				case 'modal-iframe':
					if (this.state.url !== undefined) {
						clickAction = () => { modal(this.state.url, "iframe");};
					} else {
						console.log("[MODAL] Al botón " + (id !== undefined ? '#' + id + ' ' : '') + "le falta la propiedad 'url'");
					}
					break;
			
				case 'modal-question':
					if (this.state.text !== undefined) {
						if (this.state.callback !== undefined){
							clickAction = () => { modal(this.state.text, "question", this.state.callback);};
						} else {
							console.log("[MODAL] Al botón " + (id !== undefined ? '#' + id + ' ' : '') + "le falta la propiedad 'callback'");
						}
					} else {
						console.log("[MODAL] Al botón " + (id !== undefined ? '#' + id + ' ' : '') + "le falta la propiedad 'text'");
					}
					break;

				default:
					break;
			}

			this.setState({ 
				className: className + "modal",
				clickAction: clickAction
			});

		} else {
			
			this.setState({ 
				className: className + (this.state.type || '')});
		}

	}

	doClickAction(e){
		let classes = this.state.className.trim();
		
		this.setState({
			className: classes + ' snd-clicked'
		});

		if (this.state.clickAction !== null){
			this.state.clickAction();
			if (this.props.clickActionText !== undefined && this.props.clickActionText.length > 0) {
				toast(this.props.clickActionText);
			}
		}

		setTimeout(() => {
			this.setState({
				className: classes
			});
		}, 200);
	}

	generateContent(){
		let icon;

		switch (this.state.type) {
			case 'back':
				icon = <span className="text"><FontAwesome name="chevron-left" /> {this.props.text} </span>
				break;

			case 'save':
				icon = <span className="text"><FontAwesome name="save" /> {this.props.text} </span>
				break;

			case 'download':
				icon = <span className="text"><FontAwesome name="download" /> {this.props.text} </span>
				break;

			case 'generate':
				icon = <span className="text"><FontAwesome name="cog" /> {this.props.text} </span>
				break;

			case 'copy':
				icon = <span className="text"><FontAwesome name="copy" /> {this.props.text} </span>
				break;

			case 'doc':
				icon = <span className="text"><FontAwesome name="file-alt" /> {this.props.text} </span>
				break;

			case 'pdf':
				icon = <span className="text"><FontAwesome name="file-pdf" /> {this.props.text} </span>
				break;

			case 'ppt':
				icon = <span className="text"><FontAwesome name="file-powerpoint" /> {this.props.text} </span>
				break;

			case 'xls':
				icon = <span className="text"><FontAwesome name="file-excel" /> {this.props.text} </span>
				break;

			case 'zip':
				icon = <span className="text"><FontAwesome name="file-archive" /> {this.props.text} </span>
				break;
			default:
				icon = <span className="text">{this.props.text} </span>
				break;
		}

		return icon;
	}

	render() {		
		return (
			<button 
				className={this.state.className + (this.props.center ? ' center' : '')} 
				onClick={(e) => {this.doClickAction(e)} }
				value={this.state.dbAction.length > 0 ? this.state.dbAction : '' }
			>
				{this.generateContent()}
			</button>
		);
	}
}

sndButton.propsTypes = {
	id: PropTypes.string,
	type: PropTypes.string,
	text: PropTypes.string,
	url: PropTypes.string,
	callback: PropTypes.func,
	clickAction: PropTypes.func,
	clickActionText: PropTypes.string
}

export default sndButton;