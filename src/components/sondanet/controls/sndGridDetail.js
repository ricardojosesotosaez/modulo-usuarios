import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome';

import { Form, Title, Input, Radio, Checkbox, Button } from '../../sondanet';

class sndGridDetail extends Component {

	constructor(props){
		super(props);

		this.showFields = this.showFields.bind(this);

		this.state = {
			item: props.item,
			fields: null
		};
	}

	componentDidMount(){
		this.showFields();
	}

	showFields(){
		let item = this.state.item;
		let fieldsArr = [];
		let cont = 0;
				
		if(this.props.editable !== undefined ){
			if(typeof(this.props.editable) === 'boolean'){
				for (const attr in item) {
					if (item.hasOwnProperty(attr)) {
						let fieldDetail = this.props.attrsDetail[cont];

						let label = this.props.labels !== undefined && this.props.labels[attr] !== undefined ? this.props.labels[attr] : attr;
						let field;

						switch (fieldDetail.type) {
							case 'radio':
								field = <Radio key={"snd-grid-detail-" + attr} name={attr} label={label} value={''+item[attr]} selected={fieldDetail.selected ? true : false } />;
								break;

							case 'checkbox':
								field = <Checkbox key={"snd-grid-detail-" + attr} name={attr} label={label} value={''+item[attr]} selected={fieldDetail.selected === item[attr] ? true : false }/>
								break;
							default:
								field = <Input key={"snd-grid-detail-" + attr} type="text" name={attr} label={label} value={''+item[attr]} />;
								break;
						}
						fieldsArr.push(field);
						cont++;
					}
				}
			}
		} else {
			for (const attr in item) {
				if (item.hasOwnProperty(attr)) {
					let label = this.props.labels !== undefined && this.props.labels[attr] !== undefined ? this.props.labels[attr] : attr;
					let field = <Input key={"snd-grid-detail-" + attr} type={this.props.attrsDetail[cont].type} name={attr} label={label} value={''+item[attr]} disabled/>;
					fieldsArr.push(field);
					cont++;
				}
			}
		}
		
		this.setState({fields: fieldsArr});
	}
	
	render() {		
		return (
			<div className="snd-grid-detail-container">
				<div className="snd-grid-detail">
					<Title text={this.props.title} />
					<FontAwesome name="times-circle" size="2x" className="snd-grid-detail-close" onClick={() => {this.props.closeDetail()}}/>
					{
						this.props.editable ? 
						<Form name="formUsuarios" apiUrl={this.props.apiUrl}>
							{this.state.fields}	
							<Button type="save" text="Guardar" dbAction="post" center/> 
						</Form>
						: 
						this.state.fields
					}
				</div>
			</div>
		);
	}
}

export default sndGridDetail;
