import React, { Component } from 'react';
import PropTypes from 'prop-types';

class sndRadio extends Component {

	render() {
		return (
			<div className="form-check">
				<input
					ref={(input) => this.input = input}
					id={'idInputRadio_' + this.props.name}
					name={this.props.name}
					type="radio"
					className="form-check-input"
					value={this.props.value}
					defaultChecked={this.props.selected ? true : false}
					disabled={this.props.disabled ? 'disabled' : ''}
				/>
				<label className="form-check-label" htmlFor={this.props.name}>{this.props.label}</label>
			</div>
		);
	}
}

sndRadio.propsTypes = {
	label: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	value: PropTypes.string.isRequired,
	selected: PropTypes.bool,
	disabled: PropTypes.bool
}

export default sndRadio;