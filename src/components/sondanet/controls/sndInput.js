import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import 'moment/locale/es';
import DatePicker from 'react-datepicker';
import FontAwesome from 'react-fontawesome';

class sndInput extends Component {

	constructor(props){
		super(props);

		this.onBlur = this.onBlur.bind(this);
		this.onChange = this.onChange.bind(this);
		this.currencyFormat = this.currencyFormat.bind(this);
		this.emailValidation = this.emailValidation.bind(this);
		this.rutValidation = this.rutValidation.bind(this);
		this.nitValidation = this.nitValidation.bind(this);
		this.dateChange = this.dateChange.bind(this);

		moment.locale('es');

		this.state = {
			valid: true,
			errorMsg: "",
			startDate: moment(),
			triggerValidation: props.validate
		};
	}

	componentDidMount(){
		if (this.props.type === 'multi' && this.props.placeholder !== undefined && this.props.placeholder.length > 0){
			this.input.value = this.props.placeholder;
		} else if(this.props.value !== undefined && this.props.value.length > 0){
			this.input.value = this.props.value;
		}

		if(this.props.focus){
			this.input.focus();
		}
	}

	onBlur(e){
		e.preventDefault();

		// Required
		if (this.props.required){
			if (this.input.value.length === 0){
				this.setState({
					valid: false,
					errorMsg: "Este campo es obligatorio"
				});
			}
		}

		// Currency
		if (this.props.type === 'currency' && this.input.value.length > 0) {
			if (isNaN(parseFloat(this.input.value.replace(/\.|,/g, '')))){
				this.setState({
					valid: false,
					errorMsg: "Debe ingresar un valor numérico"
				});
			} else {
				let opciones = {
					type: this.props.currency,
					decimales: this.props.currency === 'dolar' ? 2 : 0,
					enteros: undefined,
					separadorGrupos: this.props.currency === 'dolar' ? ',' : '.',
					separadorDecimales: this.props.currency === 'dolar' ? '.' : ','
				};
				this.input.value = this.currencyFormat(this.input.value, opciones);
			}
		}

		// RUT - NIT
		if ((this.props.type === 'rut' || this.props.type === 'nit') && this.input.value.length > 0) {
			switch (this.props.type) {
				case 'rut':
					this.rutValidation();
					break;
				case 'nit':
					this.nitValidation();
					break;
				default:
					break;
			}
		} else if (this.props.type === 'rut' && this.input.value.length === 0) {
			this.setState({
				valid: true,
				errorMsg: ""
			});
		}
		
	}

	onChange(e){
		e.preventDefault();

		// Required
		if (this.props.required) {
			if (this.input.value.length > 0) {
				this.setState({ 
					valid: true,
					errorMsg: ""
				});
			}
		}

		// Email
		if (this.props.type === 'email' && this.input.value.length > 0) {
			this.emailValidation();
		} else if (this.props.type === 'email' && this.input.value.length === 0){
			this.setState({
				valid: true,
				errorMsg: ""
			});
		}

		// Currency
		if (this.props.type === 'currency' && this.input.value.length === 0) {
			this.setState({
				valid: true,
				errorMsg: ""
			});
		}

		// RUT - NIT
		if ((this.props.type === 'rut' || this.props.type === 'nit') && this.input.value.length === 0) {
			this.setState({
				valid: true,
				errorMsg: ""
			});
		}

	}
	
	currencyFormat(num, opciones) {
		if(opciones.type === 'dolar'){
			num = num.replace(/,/g, '');	
		} else {
			num = num.replace(/\.|,/g, '');
		}
		let n = opciones.decimales;
		let x = opciones.enteros;
		let s = opciones.separadorGrupos;
		let c = opciones.separadorDecimales;

		let re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')';
		num = parseFloat(num).toFixed(Math.max(0, ~~n));

		return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
	}

	emailValidation(){
		let pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/; 

		if (!pattern.test(this.input.value)){
			this.setState({
				valid: false,
				errorMsg: "Debe ingresar un Email válido"
			});
		} else {
			this.setState({
				valid: true,
				errorMsg: ""
			});
		}
	}

	rutValidation(){
		let rut = this.input.value;
		let resp = false;
		
		if( rut.indexOf('-') === -1  ){
			rut = rut.substr(0, (rut.length - 1)) + '-' + rut.substr(rut.length - 1);
		}

		let suma = 0;
		let arrRut = rut.split("-");
		let rutSolo = arrRut[0];
		let verif = arrRut[1];
		let continuar = true;
		for (let i = 2; continuar; i++) {
			suma += (rutSolo % 10) * i;
			rutSolo = parseInt((rutSolo / 10), 10);
			i = (i === 7) ? 1 : i;
			continuar = (rutSolo === 0) ? false : true;
		}

		let resto = suma % 11;
		let dv = 11 - resto;
		
		if (dv === 10) {
			if (verif.toUpperCase() === 'K'){
				resp = true;
			}
		} else if (dv === 11 && parseInt(verif, 10) === 0){
			resp = true;
		} else if (dv === parseInt(verif, 10)){
			resp = true;
		}

		if(!resp){
			this.setState({
				valid: false,
				errorMsg: "Debe ingresar un RUT válido"
			});
		} else {
			this.setState({
				valid: true,
				errorMsg: ""
			});
		}
		
		this.input.value = rut;
	}

	nitValidation() {
		let nitText = this.input.value;

		if (nitText.indexOf('-') === -1) {
			nitText = nitText.substr(0, (nitText.length - 1)) + '-' + nitText.substr(nitText.length - 1);
		}

		var tmpvec = nitText.split('-');

		if (tmpvec.length < 2) {
			this.setState({
				valid: false,
				errorMsg: "Debe ingresar un NIT válido"
			});
		}

		var nit, dnit;

		nit = nitText.substring(0, nitText.length - 1);
		dnit = nitText.substring(nitText.length, nitText.length - 1);

		if (nit.length < 1) {
			this.setState({
				valid: false,
				errorMsg: "El formato del NIT es incorrecto"
			});
		}

		var str = nit.replace("-", "").replace(".", "");

		if (isNaN(parseInt(str, 10)) || str.length > 10) {
			this.setState({
				valid: false,
				errorMsg: "El formato del NIT es incorrecto"
			});
		}

		var nums = [3, 7, 13, 17, 19, 23, 29, 37, 41, 43, 47, 53, 59, 67, 71],
			i = 0,
			j = 0,
			sum = 0;

		for (var index = str.length; index > 0; index--) {
			sum = sum + (parseInt(str.substring(i, 1), 10) * nums[j]);
			j++;
		}

		var dv;

		if (sum % 11 > 1) {
			dv = 11 - (sum % 11);
		} else {
			dv = sum % 11;
		}

		if (dv === parseInt(dnit, 10)) {
			this.setState({
				valid: true,
				errorMsg: ""
			});
		} else {
			this.setState({
				valid: false,
				errorMsg: "Debe ingresar un NIT válido"
			});
		}

		this.input.value = nitText;
	}

	dateChange(date){
		// e.preventDefault();
		this.setState({
			startDate: date
		});
	}

	render() {

		let currencySign;
		
		if (this.props.type === 'currency'){
			currencySign = '$';
			if (this.props.currency !== undefined && this.props.currency.length > 0) {
				switch (this.props.currency) {
					case 'peso':
						currencySign = '$';
						break;
					case 'dolar':
						currencySign = 'US$';
						break;
					default:
						currencySign = '$';
						break;
				}
			}
		}
		
		return (
			<div className="form-group">
				<label htmlFor={this.props.name}>
					{this.props.label}
					{
						this.props.required
						?
						<span className="required">*</span>
						: null
					}
				</label>
				{
					this.props.type === 'date'
						?
						<DatePicker
							selected={this.state.startDate}
							selectsEnd
							onChange={this.dateChange}
							dateFormat={this.props.dateFormat || "DD/MM/YYYY"}
							todayButton={"Hoy"}
							disabled={false}
							className="form-control"
						/>
						: null
				}
				
				<div className="input-group">
					{
						this.props.type === 'email'
							?
							<div className="input-group-prepend">
								<span className="input-group-text">@</span>
							</div>
							: null
					}
					{
						this.props.type === 'currency'
							?
							<div className="input-group-prepend">
								<span className="input-group-text">{currencySign}</span>
							</div>
							: null
					}
					{
						this.props.type === 'rut' || this.props.type === 'nit'
							?
							<div className="input-group-prepend">
								<span className="input-group-text">{this.props.type.toUpperCase()}</span>
							</div>
							: null
					}
					{
						this.props.type === 'login-usuario'
							?
								<FontAwesome name="user"/>
							: null
					}
					{
						this.props.type === 'password'
							?
							<FontAwesome name="key" />
							: null
					}
					{
						this.props.type !== 'date' && this.props.type !== 'multi' && this.props.type !== 'select'
						?
							<input
								ref={(input) => this.input = input}
								type={this.props.type === 'currency' || this.props.type === 'rut' || this.props.type === 'nit' || this.props.type === 'date' || this.props.type === 'login-usuario' ? 'text' : this.props.type}
								name={this.props.name}
								id={'idInput_' + this.props.name}
								placeholder={this.props.placeholder}
								required={this.props.required}
								className={'snd-input form-control ' + (this.state.valid ? '' : 'error')}
								onBlur={(e) => { this.onBlur(e) }}
								onChange={(e) => { this.onChange(e) }}
								disabled={this.props.disabled ? 'disabled' : ''}
							/>
						: null
					}
					{
						this.props.type === 'multi'
							?
							<textarea 
								ref={(input) => this.input = input}
								id={'idInput_' + this.props.name}
								className={'form-control ' + (this.state.valid ? '' : 'error')}
								onBlur={(e) => { this.onBlur(e) }}
								onChange={(e) => { this.onChange(e) }}
							></textarea>
							: null
					}
					{
						this.props.type === 'select'
							?
							<select 
								className="form-control" 
								id={'idInput_' + this.props.name}
								ref={(input) => this.input = input}
								className={'form-control ' + (this.state.valid ? '' : 'error')}
								onBlur={(e) => { this.onBlur(e) }}
								onChange={(e) => { this.onChange(e) }}
							>
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
							</select>
							: null
					}
				</div>
				{
					!this.state.valid
					?
					<small className="form-text error-msg">{this.state.errorMsg}</small>
					: null
				}
			</div>
		)
	}
}

sndInput.propsTypes = {
	type: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	placeholder: PropTypes.string,
	required: PropTypes.string
};

export default sndInput;