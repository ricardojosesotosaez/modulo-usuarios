// Libraries
import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SearchBox extends Component {

	constructor(props) {
		super(props);

		this.onChange = this.onChange.bind(this);
		this.onFocus = this.onFocus.bind(this);
		this.onBlur = this.onBlur.bind(this);
		this.onItemClick = this.onItemClick.bind(this);
		this.onKeyDown = this.onKeyDown.bind(this);

		this.state = {
			items: [],
			listItems: [],
			hiddenList: true,
			valid: true
		};
	}

	componentWillMount() {
		let items = this.props.items;
		let listItems = [];
		items.forEach((item, index) => {
			listItems.push(
				<li key={index} data-text={item} onClick={() => { this.onItemClick(item) }}>
					{item}
				</li>);
		});

		this.setState({
			items: this.props.items,
			listItems: listItems
		});

	}

	onChange(e) {
		e.preventDefault();

		let items = this.state.items,
			filteredItems = [];

		items.forEach((item, index) => {
			if (item.trim().toLowerCase().indexOf(this.input.value.trim().toLocaleLowerCase()) > -1) {

				filteredItems.push(
					<li key={index} data-text={item} onClick={() => { this.onItemClick(item) }}>
						{item}
					</li>
				);
			}
		});

		this.setState({
			listItems: filteredItems
		});

	}

	onFocus(e) {
		e.preventDefault();
		setTimeout(() => {
			this.setState({ hiddenList: false });
		}, 100);
	}

	onBlur(e) {
		e.preventDefault();
		setTimeout(() => {
			this.setState({ hiddenList: true });
		}, 100);

		// Required
		if (this.props.required) {
			if (this.input.value.length === 0) {
				this.setState({
					valid: false,
					errorMsg: "Este campo es obligatorio"
				});
			}
		}
	}

	onItemClick(text) {
		let hide = !this.state.hiddenList;
		this.setState({
			hiddenList: hide
		});
		this.input.value = text;
	}

	onKeyDown(e) {
		let hide = !this.state.hiddenList;
		if (e.keyCode === 9 && this.state.listItems[0] !== undefined) {
			this.setState({
				hiddenList: hide
			});
			this.input.value = this.state.listItems[0].props["data-text"]
		}
	}

	render() {

		return (
			<div className="form-group snd-searchbox">
				<div className="input-group">
					<label htmlFor={this.props.name}>{this.props.label}</label>
					<input
						type="text"
						ref={(input) => this.input = input}
						onChange={(e) => { this.onChange(e) }}
						onFocus={(e) => { this.onFocus(e) }}
						onBlur={(e) => { this.onBlur(e) }}
						onKeyDown={(e) => { this.onKeyDown(e) }}
						className={'form-control ' + (this.state.valid ? '' : 'error')}
						name={this.props.name}
						id={'idSearchBox_' + this.props.name}
						placeholder={this.props.placeholder}
						disabled={this.props.disabled ? 'disabled': ''}
					/>
					{
						this.state.listItems.length > 0
							?
							<ul className={this.state.hiddenList ? 'hide' : ''}>
								{this.state.listItems}
							</ul>
							:
							<span className="error">No existe esa opción</span>
					}
				</div>
				{
					!this.state.valid
						?
						<small className="form-text error-msg">{this.state.errorMsg}</small>
						: null
				}
			</div>
		)
	}
}

SearchBox.propsTypes = {
	label: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	items: PropTypes.array.isRequired
}

export default SearchBox;