import React, { Component } from 'react';
import PropTypes from 'prop-types';

class sndLabel extends Component {

	render() {
		return (
			<label htmlFor={this.props.htmlFor}>
				{this.props.children}
			</label>
		);
	}
}

sndLabel.propsTypes = {
	htmlFor: PropTypes.string.isRequired
}

export default sndLabel;