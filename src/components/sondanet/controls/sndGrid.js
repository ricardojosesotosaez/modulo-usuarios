// LIBRERÍAS
import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome';

import { toast } from '../helpers';
import { GridDetail } from '../../sondanet';
import { mainPass, appConf } from '../../../config';

import encrypter from 'object-encrypter';
const encrypterEngine = encrypter(mainPass);

class sndGrid extends Component {

	constructor(props){
		super(props);

		// BINDING DE LAS FUNCIONES
		this.showHeaders = this.showHeaders.bind(this);
		this.showBody = this.showBody.bind(this);
		this.fixWidth = this.fixWidth.bind(this);
		this.showPagination = this.showPagination.bind(this);
		this.paginationClick = this.paginationClick.bind(this);
		this.paginationArrows = this.paginationArrows.bind(this);
		this.scrollable = this.scrollable.bind(this);
		this.sortingOptions = this.sortingOptions.bind(this);
		this.sortingOrder = this.sortingOrder.bind(this);
		this.scrollToTop = this.scrollToTop.bind(this);
		this.searchFocus = this.searchFocus.bind(this);
		this.searchBlur = this.searchBlur.bind(this);
		this.filterData = this.filterData.bind(this);
		this.resetPagination = this.resetPagination.bind(this);
		this.showDetail = this.showDetail.bind(this);
		this.closeDetail = this.closeDetail.bind(this);
		this.clickWithData = this.clickWithData.bind(this);
		this.clickWithoutData = this.clickWithoutData.bind(this);

		// STATE
		this.state = {
			id: props.id,
			columns: undefined,
			columnsNames: [],
			columnsTitles: [],
			dataSource: props.dataSource,
			mainData: props.mainData,
			pagination: props.pagination,
			scrollable: props.scrollable,
			sortable: props.sortable,
			searchable: props.searchable,
			searchOpen: false,
			rowsPages: [],
			detail: null,
			detailComponent: null,
			detailTitle: "",
			detailFields: [],
			detailFieldsNames: [],
			detailFieldsLabels: [],
			detailApiUrl: "",
			childComponent: null,
			hasLinks: false
		};
	}

	// FUNCIONES DE REACT
	componentWillMount(){
		let gridChildrens = this.props.children;
		let columns, detailTitle, detailApiUrl, detailFields;
		
		if(gridChildrens[0] !== undefined){
			gridChildrens.forEach((children, index) => {
				if(children.type === 'columns'){
					columns = children.props.children;
				}

				if(children.type === 'detail'){
					detailTitle = children.props.title;
					detailApiUrl = children.props.apiUrl;
					detailFields = children.props.children;
				}
			});
		} else if(gridChildrens.type === 'columns'){
			columns = gridChildrens.props.children;
		}

		if(columns){
			this.setState({columns});
			columns.forEach((col, index) => {
				this.state.columnsNames.push(col.props.name);
				this.state.columnsTitles.push(col.props.title);
			});
		}

		if(detailFields && detailFields.length > 0 && detailFields[0] !== undefined){
			detailFields.forEach((field, index) => {
				let fieldObj = {
					name: field.props.name,
					label: field.props.label,
					type: field.props.type,
					disable: field.props.disable ? field.props.disable : undefined,
					required: field.props.required ? field.props.required : undefined,
					selected: field.props.selected ? field.props.selected : undefined
				};
				this.state.detailFields.push(fieldObj);
				this.state.detailFieldsNames.push(fieldObj.name);
				this.state.detailFieldsLabels.push(fieldObj.label);
			});
			
			if(detailTitle && detailTitle.length > 0){
				this.setState({detailTitle, detailApiUrl});
			}
		} else {
			this.setState({
				childComponent: detailFields
			});
		}
		
	}

	componentDidMount(){
		this.showBody(this.state.dataSource);
		this.sortingOptions();
	}

	// Despliega los títulos en la cabecera de la grilla
	showHeaders(){
		// Columnas que se van a desplegar
		const columnsArr = this.state.columnsNames;
		// Títulos personalizados para cada columna
		let columnsNamesArr = this.state.columnsTitles !== undefined ? this.state.columnsTitles : null;
		// Variable donde se almacenan los  para la grilla
		let titleArr = [];

		// Se definen los títulos para cada columna
		// Si existe un nombre personalizado en el mismo índice, se utiliza como título
		columnsArr.forEach((col, index) => {
			let value;
			value = columnsNamesArr !== null && columnsNamesArr[index] !== undefined ? columnsNamesArr[index] : col;
			titleArr.push(<div className="snd-grid-item" key={index}>{value}</div>);
		});
		
		return titleArr;
	}

	// Despliega los elementos del cuerpo de la grilla
	// Recibe como parámetro un arreglo de objetos
	showBody(dataSource){
		
		// Se almacenan las columnas, las cuales se utilizarán para
		// mostrar solo esas propiedades de los objetos en el arreglo dataSource
		const columnsArr = this.state.columnsNames;
		const columnsDefinition = this.state.columns;
		
		// itemArr: Arreglo de los items de cada fila
		// rowsArr: Arreglo de filas
		let itemArr = [],
			rowsArr = [];

		/* 
			filtersArr: Arreglo con los filtros (campos del formulario que referencia a la grilla)
			filtersEmpty: Variable que permite identificar cuando existen o no filtros. 
						  Esta permite utilizar la grilla independiente de un formulario
		*/
		let filtersArr,
			filtersEmpty = true;
			
		if (this.state.mainData !== undefined && this.state.mainData.objData !== undefined){
			filtersArr = [];
			let objDataArr = [];

			for (const key in this.state.mainData.objData) {
				if (this.state.mainData.objData.hasOwnProperty(key)) {
					objDataArr.push([key, this.state.mainData.objData[key]]);
				}
			}
			
			objDataArr.forEach(filter => {
				if(filter[1].length > 0){
					filtersEmpty = false;
					filtersArr.push(filter);
				}
			});
		}

		if(dataSource !== undefined){
			
			dataSource.forEach((dataItem, dataIndex) => {
				
				let foundArr = [];

				if (filtersArr !== undefined && filtersEmpty === false) {
					filtersArr.forEach(filter => {
						if (dataItem[filter[0]].toLowerCase().indexOf(filter[1].toLowerCase()) > -1) {
							foundArr.push(true);
							return false;
						}
					});
				} 

				let link;
				columnsArr.forEach((col, colIndex) => {
					link = columnsDefinition[colIndex].props['link'];
					
					if(dataItem[col] !== undefined || link){

						let columnsNamesArr = this.state.columnsTitles !== undefined ? this.state.columnsTitles : undefined;
						let mobileTitle = columnsNamesArr !== null && columnsNamesArr[colIndex] !== undefined ? columnsNamesArr[colIndex] : col;
						let item;

						if(link){
							let linkText = columnsDefinition[colIndex].props['linkText'];

							let linkUrl, dataItemEnc;
							
							if(link === 'email'){
								linkUrl = 'mailto:' + dataItem[col];
							} else {
								if(columnsDefinition[colIndex].props['linkKey']){
									let urlKey = columnsDefinition[colIndex].props['linkKey'];
									linkUrl = appConf.baseUrl + columnsDefinition[colIndex].props['linkUrl'] + '/' + dataItem[urlKey];
								} else {
									dataItemEnc = encrypterEngine.encrypt(dataItem);
									linkUrl = appConf.baseUrl + columnsDefinition[colIndex].props['linkUrl'];
								}
							}

							if(!dataItemEnc){
								item = <div className="snd-grid-item" key={dataIndex + '-item-' + colIndex}><span className="snd-grid-mobile-title">{mobileTitle}:</span><a href={linkUrl} onClick={()=>{this.clickWithoutData()}}>{linkText ? linkText : dataItem[col]}</a></div>;
							} else {
								item = <div className="snd-grid-item" key={dataIndex + '-item-' + colIndex}><span className="snd-grid-mobile-title">{mobileTitle}:</span><a href={linkUrl} onClick={()=>{this.clickWithData(dataItemEnc)}}>{linkText ? linkText : dataItem[col]}</a></div>;
							}

						} else {
							item = <div className="snd-grid-item" key={dataIndex + '-item-' + colIndex}><span className="snd-grid-mobile-title">{mobileTitle}:</span>{dataItem[col]}</div>;
						}
						
						itemArr.push(item);
					}
				});

				let rowItem;

				if(link || !this.props.showDetail){
					rowItem = <div className="snd-grid-row snd-grid-body" key={'data-item-' + dataIndex}>{itemArr}</div>;
				} else{
					rowItem = <div className="snd-grid-row snd-grid-body" key={'data-item-' + dataIndex} onClick={(e) => {this.showDetail(e, dataItem)}}>{itemArr}</div>;
				}
				
				if (filtersArr !== undefined && !filtersEmpty && foundArr.length === filtersArr.length){
					rowsArr.push(rowItem);
				} else if (filtersEmpty === true){
					rowsArr.push(rowItem);
				}

				itemArr = [];

			});

			if(this.state.pagination){
				this.showPagination(rowsArr);
			} else {
				this.setState({
					gridBody: rowsArr
				});
			}
		}

		this.fixWidth();
		this.scrollable();

		columnsDefinition.forEach((colDef, index) => {
			this.setState({
				hasLinks: colDef.props['link'] ? true : false
			});
		});
		
	}

	// Muestra la paginación de la grilla
	showPagination(rows){
		if (this.state.pagination && rows !== undefined) {
			
			let paginationSize = this.props.paginationSize !== undefined && parseInt(this.props.paginationSize, 10) > 4 ? parseInt(this.props.paginationSize, 10) : 5;
			let arrPages = [];
			let rowsPages = [];
			let rowsPagesGroup = [];
			let rowsLength = rows.length;
			let pages = Math.ceil(rowsLength / paginationSize );

			rows.forEach((data, index) => {
				rowsPagesGroup.push(data);
				if (rowsPagesGroup.length === paginationSize && pages - rowsPages.length > 1){
					rowsPages.push(rowsPagesGroup);
					rowsPagesGroup = [];
				} else if (rowsPagesGroup.length < paginationSize && pages - rowsPages.length === 1 ){
					rowsPages.push(rowsPagesGroup);
				}
			});

			for (let i = 0; i < pages; i++) {
				arrPages.push(<span key={"page-" + i} className={"page-btn " + (i === 0 ? 'selected' : '')} id={"page-" + i + "-snd"} onClick={(e) => { this.paginationClick(e) }}>{i + 1}</span>);
			}

			this.setState({
				pages: arrPages,
				selectedPage: 0,
				gridBody:rowsPages[0],
				rowsPages: rowsPages
			});
		}
	}

	// Maneja los eventos de click sobre los números de la paginación
	paginationClick(e){
		let page = e.target.id.split('-')[1];
		const grid = document.getElementById(this.state.id);
		let arrSelected = grid.querySelectorAll('.selected');
		for (let i = 0; i < arrSelected.length; i++) {
			let classes = arrSelected[i].className;
			if(classes.indexOf('selected')){
				classes = classes.replace('selected', '');
			}
			arrSelected[i].className = classes;
		}

		e.target.className = e.target.className + ' selected';
		
		this.setState({
			selectedPage: parseInt(page, 10),
			gridBody: this.state.rowsPages[page]
		});

		this.scrollToTop();
	}

	// Vuelve a dejar en la primera página la paginación
	resetPagination(){
		if(this.state.rowsPages !== undefined){
			this.setState({
				gridBody: this.state.rowsPages[0]
			});
		}
		
		let arrSelected = document.querySelectorAll('.page-btn');
		
		for (let i = 0; i < arrSelected.length; i++) {
			let classes = arrSelected[i].className;
			if(classes.indexOf('selected') > -1 && arrSelected[i].id !== 'page-0-snd'){
				classes = classes.replace('selected', '');
			} else if(classes.indexOf('selected') === -1 && arrSelected[i].id === 'page-0-snd'){
				classes = classes + ' selected';
			}
			arrSelected[i].className = classes;
		}
	}

	// Maneja los eventos de tipo click en las flechas de la paginación
	paginationArrows(direction){
		const page = direction === "next" ? this.state.selectedPage + 1 : this.state.selectedPage - 1;

		if (this.state.rowsPages[page] !== undefined){
			
			const grid = document.getElementById(this.state.id);
			let arrPageBtns = grid.querySelectorAll('.page-btn');
			
			for (let i = 0; i < arrPageBtns.length; i++) {
				if (arrPageBtns[i].id.indexOf("page-" + page + "-snd") > -1){
					arrPageBtns[i].className = arrPageBtns[i].className + ' selected';
				} else {
					arrPageBtns[i].className = arrPageBtns[i].className.replace('selected', '');
				}
			}

			this.setState({
				selectedPage: parseInt(page, 10),
				gridBody: this.state.rowsPages[page]
			});

			this.scrollToTop();
		}
	}

	// En el caso que el contenido de la grilla tenga scroll, al hacer click en algún elemento de la paginación
	// vuelve a dejar la barra de scroll arriba
	scrollToTop(){
		let body = document.getElementById(this.state.id).getElementsByClassName('snd-grid-body')[0];
		body.scroll(0, 0);
	}

	// Si está activa la opción para hacer la grilla scrolleable, agrega las clases CSS necesaria para la cabecera y 
	// se define la altura máxima del contenido
	scrollable(){
		const winWidth = document.body.clientWidth;
		if(this.state.scrollable && winWidth > 767){
			setTimeout(() => {
				const gridHeader = document.getElementById(this.state.id).getElementsByClassName('snd-grid-head-container')[0];
				const gridBody = document.getElementById(this.state.id).getElementsByClassName('snd-grid-body-container')[0];
				if(gridBody && gridHeader){
					gridBody.style.maxHeight = this.props.scrollHeight ? this.props.scrollHeight + 'px' : '200px';
					if( gridHeader.className.indexOf('add-border') === -1 && gridBody.scrollHeight > parseInt(gridBody.style.maxHeight.replace('px',''), 10) ){
						gridHeader.className = gridHeader.className + ' add-border';
					}
				}
			}, 10);
		}
	}

	// Define las opciones para el ordenamiento de los datos partir de las columnas visibles
	sortingOptions(){
		if(this.state.sortable){
			const columns = this.state.columns;
			const columnsArr = this.state.columnsNames;

			let columnsNamesArr = this.state.columnsTitles !== undefined ? this.state.columnsTitles : null;

			let options = [];
			columnsArr.forEach((column, index) => {

				if(columns[index].props.link === undefined){
					let text;
					if (columnsNamesArr.length > 0 && columnsNamesArr[index] !== undefined) {
						text = columnsNamesArr[index];
					} else {
						text = column;
					}
					options.push(<option key={"sort-opt-" + index} value={column}>{text}</option>);
				}
			});

			this.setState({
				sortingOptions: options
			});
		}
	}

	// Maneja el evento de ordenamiento de los registros de la grilla
	sortingOrder(){
		const sortBy = this.sortBy.value;
		let sortedData = this.state.dataSource;

		sortedData.sort((a, b)=>{
			if( a[sortBy] < b[sortBy]){
				return -1;
			}
			if (a[sortBy] > b[sortBy]) {
				return 1;
			}
			return 0;
		});

		this.resetPagination();
		this.showBody(sortedData);
		this.scrollToTop();
		this.filterBy.value = "";
		this.searchBlur();
	}

	// Establece el foco del cursor en el campo de búsqueda
	searchFocus(){
		if (this.filterBy.className.indexOf('open') === -1){
			this.filterBy.className = 'open';
		}

		this.filterBy.focus();
	}

	// Maneja el evento blur del campo de búsqueda
	searchBlur(){
		if (this.filterBy.value.length > 0) {
			this.setState({
				searchOpen: true
			});
		} else {
			this.filterBy.className = '';
			this.setState({
				searchOpen: false
			});
		}
	}

	// Maneja el evento onChange del campo de búsqueda 
	filterData(){
		let data = this.props.dataSource;
		let filterBy = this.filterBy.value;
		let filteredData = [];

		data.forEach(ele => {
			for (const col in ele) {
				if (ele.hasOwnProperty(col)) {
					if((this.state.columnsTitles !== undefined && this.state.columnsTitles.indexOf(col) > -1) || (this.state.columnsNames !== undefined && this.state.columnsNames.indexOf(col) > -1)){
						if (ele[col] !== null){
							if (('' + ele[col].toString().toUpperCase()).indexOf(filterBy.toUpperCase()) > -1) {
								filteredData.push(ele);
								return false;
							}
						}
					}
				}
			}
		});

		this.showBody(filteredData);

	}

	// Maneja el despliegue del detalle a partir del registro seleccionado en la grilla
	showDetail(e, dataItem){
		if(this.props.showDetail && this.state.childComponent === null){
			// Variable que representa el item considerando solo los items de las columnas.
			// Se pueden definir las propiedas a desplegar a través de la propiedad attrsDetail
			let columnsNames = this.state.columnsNames;
			let columnsTitles = this.state.columnsTitles;
			let item = {};
			let labelsObj = this.state.columnsTitles !== undefined ? {} : undefined;
			let detailTitle = this.state.detailTitle;
			let detailFields = this.state.detailFields;
			let detailFieldsNames = this.state.detailFieldsNames;
			let detailFieldsLabels = this.state.detailFieldsLabels;

			for (const attr in dataItem) {
				if (dataItem.hasOwnProperty(attr)) {
					if(detailFields === undefined || detailFields.length === 0){
						if(columnsNames !== undefined && columnsNames.indexOf(attr) > -1 ){
							item[attr] = dataItem[attr];
							if(labelsObj !== undefined){
								labelsObj[attr] = columnsTitles[columnsNames.indexOf(attr)];
							}
						}
					} else if (detailFields && detailFields.length > 0) {
						if(columnsNames !== undefined && columnsNames.indexOf(attr) > -1 && detailFieldsNames.indexOf(attr) > -1){
							item[attr] = dataItem[attr];
							if(labelsObj !== undefined){
								labelsObj[attr] = detailFieldsLabels[detailFieldsNames.indexOf(attr)];
							}
						}
					}
				}
			}

			let detail = <GridDetail closeDetail={this.closeDetail} item={item} editable={this.props.editableDetail} labels={labelsObj} title={detailTitle} attrsDetail={detailFields} apiUrl={this.state.detailApiUrl}/>

			this.setState({detail});
		} else if(this.state.childComponent !== null && this.state.childComponent !== undefined) {
			let detailComponent = React.cloneElement(this.state.childComponent, {data: dataItem});
			this.setState({detailComponent});
		} else {
			toast("No existe ningún detalle para mostrar.");
			return false;
		}

		const body = document.getElementsByTagName('body');
		body[0].style.overflow = 'hidden';
		
	}

	// Cierra la ventana de detalle
	closeDetail(){
		
		const body = document.getElementsByTagName('body');
		body[0].style.overflow = 'auto';

		this.setState({
			detail: null,
			detailComponent: null
		});
	}

	clickWithData(dataItemEnc){
		sessionStorage.setItem('sndUrlData', dataItemEnc);
	}

	clickWithoutData(dataItemEnc){
		sessionStorage.removeItem('sndUrlData');
	}

	// Establece el ancho para las columnas de la grilla buscando la más ancha por columna
	fixWidth(){
		// setTimeout(() => {
		// 	let grid = document.getElementById(this.props.id);
		// 	let gridBodyRows = grid.getElementsByClassName('snd-grid-row');
		// 	let gridItems = grid.getElementsByClassName('snd-grid-item');
		// 	let items = gridBodyRows[0].getElementsByClassName('snd-grid-item').length;
		// 	let itemsWidth = [];

		// 	for (let i = 0; i < items; i++) {
		// 		itemsWidth.push(0);
		// 	}

		// 	for (let j = 0; j < gridBodyRows.length; j++) {
		// 		let gridRowItems = gridBodyRows[j].getElementsByClassName('snd-grid-item');
		// 		for (let k = 0; k < itemsWidth.length; k++) {
		// 			if(gridRowItems[k]){
		// 				itemsWidth[k] = gridRowItems[k].clientWidth > itemsWidth[k] ? gridRowItems[k].clientWidth : itemsWidth[k];
		// 			}
		// 		}
		// 	}
		// 	let cont = 0
		// 	for (let j = 0; j < gridItems.length; j++) {
		// 		gridItems[j].style.width = itemsWidth[cont] + 'px';
		// 		gridItems[j].style.maxWidth = itemsWidth[cont] + 'px';
		// 		cont = cont < 3 ? cont++ : 0;
		// 	}
			
		// }, 500);
	}

	// Función de rendereo del componente
	render() {

		let msg = "";
		if (this.state.columnsNames === undefined || this.state.columnsNames.length === 0){
			msg = 'No se han definido las columnas para mostrar para "' + this.state.id + '"';
		}

		if(this.state.gridBody === undefined){
			msg = 'No se han encontrado resultados';
		}
	
		return (
			<div className={'snd-grid-container ' + (this.props.dataSource === undefined ? 'hide' : '') + (this.state.scrollable ? ' scrollable' : '')}>
				<div id={this.state.id} className={"snd-grid-main " + (this.props.showDetail && !this.state.hasLinks ? 'click-items' : '')}>
					{
						this.state.sortable && !msg.length
							?
							<div className="snd-grid-sorting-container">
								<div className="snd-grid-sorting">
									<span>Ordenar por</span>
									<div className="sorting-selector">
										<FontAwesome name="angle-down" />
										<select ref={(input) => this.sortBy = input} onChange={() => { this.sortingOrder() }}>
											{this.state.sortingOptions}
										</select>
									</div>
								</div>
							</div>
							:
							null
					}
					{
						this.state.searchable && !msg.length
							?
							<div className="snd-grid-search-container">
								<div className="snd-grid-search">
									<FontAwesome name="search" onClick={() => { this.searchFocus() }} />
									<input
										ref={(input) => this.filterBy = input}
										type="text"
										placeholder="Búsqueda"
										onChange={() => { this.filterData() }}
										className={this.state.searchOpen ? 'open' : ''}
										onBlur={() => { this.searchBlur() }}
									/>
								</div>
							</div>
							:
							null
					}

					<div className="snd-grid-head-container">
						<div className="snd-grid-row snd-grid-head">
							{!msg.length ? this.showHeaders() : null}
						</div>
					</div>
					{
						msg.length
						?
						<div className="snd-grid-message">
							<p>{msg}</p>
						</div>
						:
						<div className="snd-grid-body-container">
							{this.state.gridBody}
						</div>
					}
					{
						this.state.pagination && this.state.pages && this.state.pages.length > 1 && msg.length === 0
							?
							<div className="pagination">
								<span
									className={"pagination-previous " + (this.state.selectedPage === 0 ? 'disable' : '')}
									onClick={() => { this.paginationArrows("previous") }}
								>
									<FontAwesome name="angle-double-left" />
								</span>
								{
									this.state.pages ? this.state.pages : null
								}
								<span
									className={"pagination-next " + (this.state.selectedPage === (this.state.rowsPages.length - 1) ? 'disable' : '')}
									onClick={() => { this.paginationArrows("next") }}
								>
									<FontAwesome name="angle-double-right" />
								</span>
							</div>
							:
							null
					}
				</div>
				{
					this.state.detailComponent !== null
					?
					<div className="snd-grid-custom-detail-overlay">
						<div className="snd-grid-custom-detail">
							<FontAwesome name="times-circle" size="2x" className="snd-grid-detail-close" onClick={() => {this.closeDetail()}}/>
							{this.state.detailComponent}
						</div>
					</div>
					:
					null
				}
				{
					this.state.detail !== null
					?
					this.state.detail
					:
					null
				}
			</div>
		);
	}
}

export default sndGrid;
