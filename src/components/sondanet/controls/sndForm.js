import React, { Component } from 'react';
import PropTypes from 'prop-types';
import request from 'superagent';

import { appConf } from '../../../config';
import { toast } from '../helpers';

import Loading from '../content/sndLoading';

class sndForm extends Component {

	constructor(props){
		super(props);

		this.showFormChildren = this.showFormChildren.bind(this);
		this.showTargetChildren = this.showTargetChildren.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.sendForm = this.sendForm.bind(this);
		this.getRequest = this.getRequest.bind(this);
		this.postRequest = this.postRequest.bind(this);

		this.state = {
			configData: appConf,
			apiUrl: props.apiUrl,
			mainApiUrl: appConf.mainApi,
			formId: 'idForm_' + this.props.name,
			resultsTable: null,
			resTarget: null,
			showLoader: false
		};
		
	}

	onSubmit(e){
		e.preventDefault();
		this.setState({
			resultsTable: null
		});
		this.validateForm();
	}

	validateForm() {
		// const method = this.state.method;
		// const service = this.state.service;
		const apiUrl = this.state.apiUrl;

		let inputs = document.getElementsByClassName('snd-input');
		let clickedButton = document.getElementsByClassName('snd-clicked');
		let dbAction = clickedButton[0].value;
		let errorCount = 0;

		if (apiUrl === undefined || apiUrl.length === 0){
			toast('Por favor, corregir configuración del formulario');
		} else {
			if (dbAction.length === 0) {
				toast('No existe una acción determinada para el formulario');
			} else {
				for (let i = 0; i < inputs.length; i++) {
					inputs[i].focus();
					inputs[i].blur();
					setTimeout(() => {
						errorCount += inputs[i].className.indexOf('error') > -1 ? 1 : 0;
					}, 1);
				}

				setTimeout(() => {
					if (errorCount > 0) {
						toast('Por favor, corregir errores en el formulario.');
					} else {
						// this.sendForm(service, method, apiUrl, inputs, dbAction);
						this.sendForm(apiUrl, inputs, dbAction);
					}
				}, 10);
			}
		}

	}

	sendForm(apiUrl, inputs, dbAction){
		const self = this;

		// Muestra loader
		self.setState({
			showLoader: true
		});
		
		// Arma objeto para post
		let mainData = {
			action: dbAction,
			objData: {}
		};

		// Guarda valores de los campos del formulario
		for (let i = 0; i < inputs.length; i++) {
			if(inputs[i].type === 'checkbox'){
				inputs[i].value = inputs[i].checked ? 'S' : 'N';
			}

			mainData.objData[inputs[i].name] = inputs[i].value;
		}

		// console.log(mainData.objData);

		// Determina la url para la petición
		let reqUrl = window.location.origin;
		if (apiUrl.length){
			reqUrl += apiUrl;
		} else {
			reqUrl += self.state.mainApiUrl;
		}

		// Evalúa que la acción definida en el botón sea válida
		if (dbAction.toLowerCase() !== 'get' && dbAction.toLowerCase() !== 'post'){
			toast('La acción "' + dbAction + '" no puede ser realizada.');
		} else if (dbAction.toLowerCase() === 'get'){
			this.getRequest(reqUrl, mainData);
		} else if (dbAction.toLowerCase() === 'post') {
			this.postRequest(reqUrl, mainData);
		}
	}

	getRequest(reqUrl, mainData) {
		const self = this;
		request
			.get(reqUrl)
			.then((res) => {

				this.props.children.forEach(element => {
					if (element.props.id === this.props.resTarget) {
						let clone = React.cloneElement(element, {
							dataSource: res.body,
							mainData: mainData
						});
						self.setState({
							resTarget: clone
						});
					}
				});
				
				self.setState({
					showLoader: false
				});
			})
			.catch(function (error) {
				toast('Ha ocurrido un error procesando la solicitud.');
				self.setState({
					showLoader: false
				});
				console.error(error);
			});
	}

	postRequest(reqUrl, mainData) {
		
		const self = this;
		request
			.post(reqUrl)
			.set('application/json')
			.send(mainData.objData)
			.then((res) => {
				self.setState({
					showLoader: false
				});
				toast(res.text);
			})
			.catch(function (error) {
				toast('Ha ocurrido un error procesando la solicitud.');
				self.setState({
					showLoader: false
				});
				console.error(error);
			});
	}

	showFormChildren(){
		if (this.props.resTarget === undefined){
			return this.props.children;
		} else {
			let formChildrensArr = [];
			if(this.props.children){
				this.props.children.forEach(element => {
					if (element.props.id !== this.props.resTarget) {
						formChildrensArr.push(element);
					}
				});
			}
			return formChildrensArr;
		}
	}

	showTargetChildren() {
		let formTarget = null;
		if(this.props.resTarget && this.props.children){
			this.props.children.forEach(element => {
				if (element !== undefined && element !== null && element.props !== undefined && element.props.id === this.props.resTarget) {
					formTarget = element;
				}
			});
		}
		return formTarget;
	}

	render() {

		let target = this.state.resTarget === null ? this.showTargetChildren() : this.state.resTarget;

		let formChildren = this.showFormChildren();

		return (
			<div className="snd-form-container">
				<form 
					noValidate
					id={this.state.formId}
					name={this.props.name}
					className={'snd-form ' + (this.props.size ? this.props.size : '') + (this.props.center ? ' center' : '')}
					onSubmit={(e) => { this.onSubmit(e)}}
				>
					{formChildren.length
					?
					formChildren
					:
					<div className="snd-message">
						<p>{'No existen campos para el formulario ' + this.props.name}.</p>
					</div>
					}
				</form>
				{
					this.state.showLoader ? <Loading /> : target
				}
			</div>
		);
	}
}

sndForm.propsTypes = {
	size: PropTypes.string
}

export default sndForm;