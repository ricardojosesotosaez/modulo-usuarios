import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';

class sndInputFile extends Component {

	constructor(props){
		super(props);

		this.onChange = this.onChange.bind(this);
		this.validateFileTypes = this.validateFileTypes.bind(this);
		this.generateFileName = this.generateFileName.bind(this);

		this.state = {
			route: "",
			name: props.name,
			filename: "",
			extension: "",
			filetypes: props.filetypes,
			valid: true,
			errorMsg: "",
			generatedFileName: 'Elegir un archivo'
		};
	}

	onChange(e){
		e.preventDefault();

		let route = this.input.value;
		let indexDot = route.split('').reverse().indexOf('.');
		indexDot = route.length - (indexDot + 1);
		let extension = route.substring(indexDot + 1, route.length);

		if (!this.validateFileTypes(extension)){
			this.setState({
				valid: false,
				errorMsg: 'La extensión ' + extension.toUpperCase() + ' no está permitida',
				filename: "",
				extension: ""
			});
			this.generateFileName("", "");
		} else {
			this.setState({
				valid: true,
				errorMsg: ""
			});
			
			let indexLastSlash = route.split('').reverse().indexOf('\\');
			indexLastSlash = route.length - (indexLastSlash + 1);
			let filename = route.substring(indexLastSlash + 1, route.length);


			this.setState({
				filename: filename,
				extension: extension
			});

			this.generateFileName(filename, extension);
		}
		
		this.input.blur();
		
	}

	validateFileTypes(extension){
		let res = false;
		if (this.state.filetypes !== undefined && this.state.filetypes.length > 0){
			let filetypes = this.state.filetypes.indexOf(',') ? this.state.filetypes.split(',') : this.state.filetypes;
			console.log(filetypes);
			if(typeof(filetypes) !== 'string'){
				filetypes.forEach((ext)=>{
					if(extension === ext.trim()){
						res = true;
					}
				});
			} else {
				if (extension === filetypes) {
					res = true;
				}
			}
		}

		return res;
	}

	generateFileName(filename, extension){

		let generatedFileName = 'Elegir un archivo';
		
		if (filename.length > 0 && extension.length) {

			switch (extension) {
				case 'doc':
					generatedFileName = <span><FontAwesome name="file-text-o" /> {filename}</span>
					break;

				case 'pdf':
					generatedFileName = <span><FontAwesome name="file-pdf-o" /> {filename}</span>
					break;

				case 'ppt':
					generatedFileName = <span><FontAwesome name="file-powerpoint-o" /> {filename}</span>
					break;

				case 'xls':
					generatedFileName = <span><FontAwesome name="file-excel-o" /> {filename}</span>
					break;

				case 'zip':
					generatedFileName = <span><FontAwesome name="file-archive-o" /> {filename}</span>
					break;

				case 'png' || 'jpg':
					generatedFileName = <span><FontAwesome name="file-image-o" /> {filename}</span>
					break;

				default:
					break;
			}
		}

		this.setState({
			generatedFileName: generatedFileName
		});
	}
	
	render() {
		
		return (
			<div className="form-group">
				<div className="input-group">
					<div className="input-group-prepend">
						<span className="input-group-text"><FontAwesome name="upload" /></span>
					</div>
					<div className="custom-file">
						<input 
							ref={(input) => this.input = input}
							type="file"
							name={this.state.name}
							id={'idInputFile_' + this.state.name}
							required={this.props.required}
							onChange={(e)=>{this.onChange(e)}}
						/>
						<label className="custom-file-label" htmlFor={'idInputFile_' + this.state.name}>
							{this.state.generatedFileName}
						</label>
					</div>
				</div>
				{
					!this.state.valid
						?
						<small className="form-text error-msg">{this.state.errorMsg}</small>
						: null
				}
			</div>
		);
	}
}

sndInputFile.propsTypes = {
	name: PropTypes.string.isRequired,
	fileTypes: PropTypes.string.isRequired,
	required: PropTypes.string
};

export default sndInputFile;
